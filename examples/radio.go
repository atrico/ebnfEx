package examples

// Valid (UK) Callsign
const callsignEBNF = `
Callsign = uk_callsign.
uk_callsign = uk_fnd_callsign|uk_int_callsign|uk_full_callsign.
uk_fnd_callsign = "M" [rsl] ("3" | "6" | "7") three_letters ["/" suffix].
uk_int_callsign = "2" ("E" | rsl) ("0" | "1") three_letters ["/" suffix].
uk_full_callsign = (((("G" | "M") [rsl] ("0" | "1")) | "G" [rsl] "7") three_letters) | ("G" [rsl] "5" two_letters) |(("G" [rsl] ("2" | "3" | "4" | "6" | "8") | "M" [rsl] "5") (three_letters | two_letters)) ["/" suffix_with_mm].
rsl = "D" | "I" | "J" | "M" | "U" | "W".
letter = "A"…"Z".
two_letters = letter letter.
three_letters = two_letters letter.
suffix = "A" | "P" | "M"
suffix_with_mm = suffix | "MM"
`

// Q code
const qcodeEBNF = `
Qcode = "Q" (("R" "M"…"P") | ("S" ("B" | "L" | "O" | "Y")) | "TH").
`
