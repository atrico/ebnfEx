module gitlab.com/atrico/ebnf/v2

go 1.24

require (
	github.com/google/uuid v1.6.0
	gitlab.com/atrico/core/v2 v2.2.5
	gitlab.com/atrico/testing/v2 v2.5.3
	golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa
	golang.org/x/net v0.35.0
)

require (
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/rs/zerolog v1.33.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
)
