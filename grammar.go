package ebnfEx

import (
	"errors"
	"fmt"
	"gitlab.com/atrico/core/v2"
	"io"
	"reflect"
	"strconv"
	"strings"
	"text/scanner"
	"unicode"

	"github.com/google/uuid"
	"gitlab.com/atrico/core/v2/collection/set"
	"golang.org/x/exp/ebnf"
	"golang.org/x/net/context"
)

type Grammar interface {
	// Get all public productions
	GetPublicProductions() []string
	// Create all tokens from specified production that matches grammar rules
	CreateAll(ctx context.Context, production string, options ...CreateOption) (tokens <-chan string)
	// Create a random token from specified production that matches grammar rules
	CreateRandom(production string, randomGen func(max int) int, options ...CreateOption) (token string)
	// Verify (single production)
	Verify(production string) error
	// Verify all public productions
	VerifyAll() error
	// Validate a token against a specific production
	Validate(production string, token string) (isValid bool)
	// Validate a token against all public productions
	ValidateAll(token string) (validProductions []string)
}

func MakeGrammar(grammar ebnf.Grammar) Grammar {
	return &grammarImpl{
		grammar,
		strings.Builder{},
		make(map[string]ebnf.Alternative),
	}
}

func ParseGrammar(reader io.Reader) (grammar Grammar, err error) {
	var grm ebnf.Grammar
	if grm, err = ebnf.Parse("", reader); err == nil {
		grammar = MakeGrammar(grm)
	}
	return grammar, err
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type grammarImpl struct {
	grammar ebnf.Grammar
	result  strings.Builder
	ranges  map[string]ebnf.Alternative
}

func (g *grammarImpl) GetPublicProductions() (productions []string) {
	for name, _ := range g.grammar {
		if unicode.IsUpper(rune(name[0])) {
			productions = append(productions, name)
		}
	}
	return productions
}

func (g *grammarImpl) CreateAll(ctx context.Context, production string, options ...CreateOption) (tokens <-chan string) {
	name := ebnf.Name{
		StringPos: scanner.Position{},
		String:    production,
	}
	opts := MakeOptions(options)
	ch := make(chan string)
	go g.createAllFromNode(ctx, &name, opts, ch)
	return ch
}

func (g *grammarImpl) CreateRandom(production string, randomGen func(max int) int, options ...CreateOption) string {
	name := ebnf.Name{
		StringPos: scanner.Position{},
		String:    production,
	}
	opts := MakeOptions(options)
	return g.createRandomFromNode(&name, randomGen, opts)
}

func (g *grammarImpl) Verify(production string) error {
	return ebnf.Verify(g.grammar, production)
}

func (g *grammarImpl) VerifyAll() (err error) {
	// Temporary production
	tmpId := fmt.Sprintf("Tmp%s", strings.Replace(uuid.New().String(), "-", "_", -1))
	// Get all public productions
	var public ebnf.Sequence
	for _, name := range g.GetPublicProductions() {
		public = append(public, &ebnf.Name{String: name})
	}
	if len(public) > 0 {
		// Add temp "reference all" production, then test it
		g.grammar[tmpId] = &ebnf.Production{Name: &ebnf.Name{String: tmpId}, Expr: public}
		defer delete(g.grammar, tmpId)
		err = g.Verify(tmpId)
	}
	return err
}

func (g *grammarImpl) Validate(production string, token string) (isValid bool) {
	name := ebnf.Name{
		StringPos: scanner.Position{},
		String:    production,
	}
	// Check there is a solution that uses all the token
	solutions := g.validateFromNode(&name, token, 0)
	for remaining := range solutions.Iterator() {
		if remaining == len(token) {
			return true
		}
	}
	return false
}

func (g *grammarImpl) ValidateAll(token string) (validProductions []string) {
	for _, production := range g.GetPublicProductions() {
		if g.Validate(production, token) {
			validProductions = append(validProductions, production)
		}
	}
	return validProductions
}

func (g *grammarImpl) createAllFromNode(ctx context.Context, currentNode ebnf.Expression, options createOptions, ch chan string) {
	deferCloseChannel := false
	defer func() {
		if !deferCloseChannel {
			close(ch)
		}
	}()
	select {
	case <-ctx.Done():
		return
	default:
		switch node := currentNode.(type) {
		case ebnf.Alternative:
			for _, nd := range node {
				ch2 := make(chan string)
				go g.createAllFromNode(ctx, nd, options, ch2)
				for tk := range ch2 {
					ch <- tk
				}
			}
		case *ebnf.Group:
			deferCloseChannel = true
			g.createAllFromNode(ctx, node.Body, options, ch)
		case *ebnf.Name:
			deferCloseChannel = true
			g.createAllFromNode(ctx, g.grammar[node.String].Expr, options, ch)
		case *ebnf.Option:
			ch <- ""
			deferCloseChannel = true
			g.createAllFromNode(ctx, node.Body, options, ch)
		case *ebnf.Repetition:
			if options.minRepeats == 0 {
				ch <- ""
				options.minRepeats = 1
			}
			if options.maxRepeats >= options.minRepeats {
				ch2 := make(chan string)
				go g.createAllFromNode(ctx, node.Body, options, ch2)
				repeater := core.ReadChannelAsSlice(ch2)
				for repeats := options.minRepeats; repeats <= options.maxRepeats; repeats++ {
					for _, rep := range repeater {
						result := strings.Builder{}
						for i := uint(0); i < repeats; i++ {
							result.WriteString(rep)
						}
						ch <- result.String()
					}
				}
			}
		case ebnf.Sequence:
			combinations := func(sequence [][]string) (tokens []string) {
				if len(sequence) > 0 {
					index := make([]int, len(sequence))
					last := len(sequence) - 1
					for index[last] < len(sequence[last]) {
						result := strings.Builder{}
						for i, tok := range sequence {
							result.WriteString(tok[index[i]])
						}
						tokens = append(tokens, result.String())
						for i := range index {
							if i > 0 {
								index[i-1] = 0
							}
							index[i] = index[i] + 1
							if index[i] < len(sequence[i]) {
								break
							}
						}
					}
				}
				return tokens
			}
			sequence := make([][]string, len(node))
			for i, n := range node {
				ch2 := make(chan string)
				go g.createAllFromNode(ctx, n, options, ch2)
				sequence[i] = core.ReadChannelAsSlice(ch2)
			}
			for _, tk := range combinations(sequence) {
				ch <- tk
			}
		case *ebnf.Range:
			items := g.getRange(node)
			deferCloseChannel = true
			g.createAllFromNode(ctx, items, options, ch)
		case *ebnf.Token:
			ch <- node.String
		default:
			panic(fmt.Sprintf("unknown grammar type: %v", reflect.TypeOf(currentNode)))
		}
	}
}

func (g *grammarImpl) createRandomFromNode(currentNode ebnf.Expression, rand func(max int) int, options createOptions) string {
	result := strings.Builder{}
	switch node := currentNode.(type) {
	case ebnf.Alternative:
		result.WriteString(g.createRandomFromNode(node[rand(len(node))], rand, options))
	case *ebnf.Group:
		result.WriteString(g.createRandomFromNode(node.Body, rand, options))
	case *ebnf.Name:
		result.WriteString(g.createRandomFromNode(g.grammar[node.String].Expr, rand, options))
	case *ebnf.Option:
		if rand(2) > 0 {
			result.WriteString(g.createRandomFromNode(node.Body, rand, options))
		}
	case *ebnf.Repetition:
		repeats := uint(rand(int(options.maxRepeats-options.minRepeats))) + options.minRepeats
		for i := uint(0); i <= repeats; i++ {
			result.WriteString(g.createRandomFromNode(node.Body, rand, options))
		}
	case ebnf.Sequence:
		for _, n := range node {
			result.WriteString(g.createRandomFromNode(n, rand, options))
		}
	case *ebnf.Range:
		items := g.getRange(node)
		result.WriteString(g.createRandomFromNode(items, rand, options))
	case *ebnf.Token:
		result.WriteString(node.String)
	default:
		panic(fmt.Sprintf("unknown grammar type: %v", reflect.TypeOf(currentNode)))
	}
	return result.String()
}

func (g *grammarImpl) validateFromNode(currentNode ebnf.Expression, token string, idx int) set.Set[int] {
	newIdx := set.MakeMutableSet[int]()
	switch node := currentNode.(type) {
	case ebnf.Alternative:

		// Add each viable alternative
		for _, alt := range node {
			newIdx.ToUnion(g.validateFromNode(alt, token, idx))
		}
	case *ebnf.Group:
		return g.validateFromNode(node.Body, token, idx)
	case *ebnf.Name:
		return g.validateFromNode(g.grammar[node.String].Expr, token, idx)
	case *ebnf.Option:
		// No option
		newIdx.Add(idx)
		// Add option?
		newIdx.ToUnion(g.validateFromNode(node.Body, token, idx))
	case *ebnf.Repetition:
		// As many as possible (from 0)
		var inpt set.Set[int]
		outpt := set.MakeMutableSet[int](idx)
		for outpt.Len() > 0 {
			newIdx.ToUnion(outpt)
			inpt = outpt
			outpt = set.MakeMutableSet[int]()
			for inp := range inpt.Iterator() {
				outpt.ToUnion(g.validateFromNode(node.Body, token, inp))
			}
		}
	case ebnf.Sequence:
		intermediate := set.MakeMutableSet[int](idx)
		for _, part := range node {
			newIdx = set.MakeMutableSet[int]()
			for prev := range intermediate.Iterator() {
				newIdx.ToUnion(g.validateFromNode(part, token, prev))
			}
			intermediate = newIdx
		}
	case *ebnf.Range:
		items := g.getRange(node)
		return g.validateFromNode(items, token, idx)
	case *ebnf.Token:
		if strings.HasPrefix(token[idx:], node.String) {
			newIdx.Add(idx + len(node.String))
		}
	default:
		panic(fmt.Sprintf("unknown grammar type: %v", reflect.TypeOf(currentNode)))
	}
	return newIdx
}

func (g *grammarImpl) getRange(rge *ebnf.Range) (alternative ebnf.Alternative) {
	name := fmt.Sprintf("%s->%s", rge.Begin.String, rge.End.String)
	var ok bool
	if alternative, ok = g.ranges[name]; !ok {
		var err error
		// Try numeric
		var start, end int
		if start, err = strconv.Atoi(rge.Begin.String); err == nil {
			if end, err = strconv.Atoi(rge.End.String); err == nil {
				if start > end {
					tmp := start
					start = end
					end = tmp
				}
				for i := start; i <= end; i++ {
					alternative = append(alternative, &ebnf.Token{String: strconv.Itoa(i)})
				}
			}
		}
		if err != nil {
			// Try ascii
			if len(rge.Begin.String) == 1 || len(rge.End.String) == 1 {
				var start uint8 = rge.Begin.String[0]
				var end uint8 = rge.End.String[0]
				if start > end {
					tmp := start
					start = end
					end = tmp
				}
				for ; start <= end; start++ {
					alternative = append(alternative, &ebnf.Token{String: string(start)})
				}
				err = nil
			} else {
				err = errors.New(fmt.Sprintf("invalid range: %s", name))
			}
		}
		if err == nil {
			g.ranges[name] = alternative
		}
	}
	return alternative
}
