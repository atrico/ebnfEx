package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	ebnfEx "gitlab.com/atrico/ebnf/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"golang.org/x/net/context"
	"strings"
	"testing"
)

func Test_AllTokens(t *testing.T) {
	for name, testCase := range createAllTestCases {
		t.Run(name, func(t *testing.T) {
			// Parse
			grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(testCase.ebnf))
			assert.That[any](t, errP, is.Nil, "parse")
			// Get all
			tokens := core.ReadChannelAsSlice(grammar.CreateAll(context.Background(), "Root"))

			// Assert
			assert.That(t, tokens, is.EquivalentTo(testCase.expected), "")
		})
	}
}

func Test_AllTokens_WithRepeatOptions(t *testing.T) {
	// Parse
	grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(`Root={"A" | "B"}.`))
	assert.That[any](t, errP, is.Nil, "parse")
	// Get all
	tokens := core.ReadChannelAsSlice(grammar.CreateAll(context.Background(), "Root", ebnfEx.MinRepeatsCreateOption(1), ebnfEx.MaxRepeatsCreateOption(2)))

	// Assert
	assert.That(t, tokens, is.EquivalentTo(str("A", "AA", "B", "BB")), "")
}

var createAllTestCases = map[string]createAllTestCase{
	"Token":                         {ebnf: `Root="A".`, expected: str("A")},
	"Sequence":                      {ebnf: `Root="A" "B" "C".`, expected: str("ABC")},
	"Alternative":                   {ebnf: `Root="A" | "B" | "C".`, expected: str("A", "B", "C")},
	"Group":                         {ebnf: `Root=("A" | "B").`, expected: str("A", "B")},
	"Sequence of Alternatives":      {ebnf: `Root=("A" | "B") ("B" | "C").`, expected: str("AB", "AC", "BB", "BC")},
	"Range":                         {ebnf: `Root="A"…"C".`, expected: str("A", "B", "C")},
	"Option":                        {ebnf: `Root="A" ["B"] "C".`, expected: str("AC", "ABC")},
	"Repetition":                    {ebnf: `Root={"A" | "B"}.`, expected: str("", "A", "AA", "AAA", "B", "BB", "BBB")},
	"Backtrack Alternative":         {ebnf: `Root=("A" | "AB") "C".`, expected: str("AC", "ABC")},
	"Backtrack Option":              {ebnf: `Root="A" ["B"] "BC".`, expected: str("ABC", "ABBC")},
	"Backtrack Repetition/Sequence": {ebnf: `Root={"A"} "A".`, expected: str("A", "AA", "AAA", "AAAA")},
}

type createAllTestCase struct {
	ebnf     string
	expected []string
}
