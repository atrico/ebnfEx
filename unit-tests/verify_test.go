package unit_tests

import (
	ebnfEx "gitlab.com/atrico/ebnf/v2"
	"strings"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Verify_Simple(t *testing.T) {
	// Arrange
	const defs = `Hello = "a" | "b" | "c".`
	grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(defs))

	// Act
	errV := grammar.Verify("Hello")
	errVA := grammar.VerifyAll()

	// Assert
	assert.That[any](t, errP, is.Nil, "parse")
	assert.That[any](t, errV, is.Nil, "verify")
	assert.That[any](t, errVA, is.Nil, "verify all")
}

func Test_Verify_Unreachable(t *testing.T) {
	// Arrange
	const defs = `Hello = "a" | "b" | "c". world = "W".`
	grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(defs))

	// Act
	errV := grammar.Verify("Hello")
	errVA := grammar.VerifyAll()

	// Assert
	assert.That[any](t, errP, is.Nil, "parse")
	assert.That[any](t, errV, is.NotNil, "verify")
	assert.That[any](t, errVA, is.NotNil, "verify all")
}

func Test_VerifyAll_TwoPublic(t *testing.T) {
	// Arrange
	const defs = `Hello = sub. World = sub. sub = "A".`
	grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(defs))

	// Act
	errV := grammar.Verify("Hello")
	errV2 := grammar.Verify("World")
	errVA := grammar.VerifyAll()

	// Assert
	assert.That[any](t, errP, is.Nil, "parse")
	assert.That[any](t, errV, is.NotNil, "verify")
	assert.That[any](t, errV2, is.NotNil, "verify2")
	assert.That[any](t, errVA, is.Nil, "verify all")
}
