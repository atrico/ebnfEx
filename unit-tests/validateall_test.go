package unit_tests

import (
	ebnfEx "gitlab.com/atrico/ebnf/v2"
	"strings"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_ValidateAll(t *testing.T) {
	for name, testCase := range validateAllTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase validateAllTestCase) {
				// Arrange
				grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(testCase.ebnf))
				assert.That[any](t, errP, is.Nil, "parse")
				// Act
				validProductions := grammar.ValidateAll(testCase.token)
				// Assert
				assert.That[[]string](t, validProductions, is.EquivalentTo(testCase.valid), "validate all")
			}(testCase)
		})
	}
}

type validateAllTestCase struct {
	ebnf  string
	token string
	valid []string
}

var validateAllTestCases = map[string]validateAllTestCase{
	"No Match":       {ebnf: `One="A".Two="B".`, token: "C", valid: str()},
	"Single Match":   {ebnf: `One="A".Two="B".`, token: "A", valid: str("One")},
	"Multiple Match": {ebnf: `One="A"|"B".Two="B".Three="C".`, token: "B", valid: str("One", "Two")},
}
