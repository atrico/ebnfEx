package unit_tests

import (
	ebnfEx "gitlab.com/atrico/ebnf/v2"
	"strings"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Validate(t *testing.T) {
	for name, testCase := range validateTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase validateTestCase) {
				// Parse
				grammar, errP := ebnfEx.ParseGrammar(strings.NewReader(testCase.ebnf))
				assert.That[any](t, errP, is.Nil, "parse")
				// Valid
				for _, item := range testCase.valid {
					valid := grammar.Validate("Root", item)
					assert.That(t, valid, is.True, "validate: %s", item)
				}
				// Invalid
				for _, item := range testCase.invalid {
					valid := grammar.Validate("Root", item)
					assert.That(t, valid, is.False, "invalidate: %s", item)
				}
			}(testCase)
		})
	}
}

type validateTestCase struct {
	ebnf    string // Production will be called "Root"
	valid   []string
	invalid []string
}

var validateTestCases = map[string]validateTestCase{
	"Token":                         {ebnf: `Root="A".`, valid: str("A"), invalid: str("", "B", "AB")},
	"Sequence":                      {ebnf: `Root="A" "B" "C".`, valid: str("ABC"), invalid: str("", "ABD", "AC", "AB", "ABCD")},
	"Alternative":                   {ebnf: `Root="A" | "B" | "C".`, valid: str("A", "B", "C"), invalid: str("", "D", "AB")},
	"Range":                         {ebnf: `Root="A"…"C".`, valid: str("A", "B", "C"), invalid: str("", "D", "AB")},
	"Group":                         {ebnf: `Root=("A" | "B") "C".`, valid: str("AC", "BC"), invalid: str("", "C", "AB", "ABC")},
	"Option":                        {ebnf: `Root="A" ["B"] "C".`, valid: str("AC", "ABC"), invalid: str("", "A", "AB", "BC", "C")},
	"Repetition":                    {ebnf: `Root={"A" | "B"}.`, valid: str("", "A", "AA", "AAA", "B", "BB", "BBB", "ABABA", "BABABABABABABAB"), invalid: str("C")},
	"Backtrack Alternative":         {ebnf: `Root=("A" | "AB") "C".`, valid: str("AC", "ABC"), invalid: str("", "C")},
	"Backtrack Option":              {ebnf: `Root="A" ["B"] "BC".`, valid: str("ABC", "ABBC"), invalid: str("", "A", "AB", "BC", "C")},
	"Backtrack Repetition/Sequence": {ebnf: `Root={"A"} "A".`, valid: str("A", "AA", "AAA"), invalid: str("", "C")},
}

func str(items ...string) []string {
	return items
}
