package ebnfEx

type CreateOption interface {
	setOptions(opt *createOptions)
}

func MinRepeatsCreateOption(val uint) CreateOption {
	return createOptionMinRepeats(val)
}
func MaxRepeatsCreateOption(val uint) CreateOption {
	return createOptionMaxRepeats(val)
}

type createOptions struct {
	minRepeats uint
	maxRepeats uint
}

func MakeOptions(options []CreateOption) (out createOptions) {
	out = createOptions{maxRepeats: 3}
	for _, option := range options {
		option.setOptions(&out)
	}
	return out
}

type createOptionMinRepeats uint

func (c createOptionMinRepeats) setOptions(opt *createOptions) {
	opt.minRepeats = uint(c)
}

type createOptionMaxRepeats uint

func (c createOptionMaxRepeats) setOptions(opt *createOptions) {
	opt.maxRepeats = uint(c)
}
